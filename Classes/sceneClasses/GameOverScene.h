#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"

class GameOverScene : public cocos2d::LayerGradient
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    CREATE_FUNC(GameOverScene);

	cocos2d::Menu* createMenu();
//private:
	void playAgain(cocos2d::Ref* pSender);
	void highScores(cocos2d::Ref* pSender);
	void mainMenu(cocos2d::Ref* pSender);
	void saveResults(const std::string& fName, const char* person);
	void addWinnerLabels();
	void addLoserLabels();
};


#endif // __GAMEOVER_SCENE_H__
