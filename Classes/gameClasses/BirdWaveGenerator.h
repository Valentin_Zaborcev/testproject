#ifndef __BIRD_WAVE_GENERATOR_H__
#define __BIRD_WAVE_GENERATOR_H__

#include "cocos2d.h"
#include "Bird.h"
#include "../configurator/Configurator.h"

class BirdWaveGenerator : public cocos2d::Layer {
protected:
	BirdWaveGenerator() {}
	BirdWaveGenerator(const BirdWaveGenerator&) {}

public:
	virtual ~BirdWaveGenerator() {}
	static BirdWaveGenerator* create();
	void gameLoop();

private:
	float _z = 0;
	float _waveTimer = config::Configurator::getInstance()->waveTimer;
	int _birdsAvgAmount = config::Configurator::getInstance()->birdsStartAmount;
	int _birdsMaxAmount = config::Configurator::getInstance()->birdsMaxAmount;
	cocos2d::Action* _currentAction;
};


#endif // __BIRD_WAVE_GENERATOR_H__